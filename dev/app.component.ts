import {Component} from 'angular2/core';

@Component({
    selector: 'my-app',
    template: `
        <h1>Angular 2 Boilerplate</h1>
        <h3 (click)="onSelect()" [class.clicked]="showDetail === true">{{contact.firstName}} {{contact.lastName}}</h3>
        <input [(ngModel)]="contact.lastName" type="text">
        <div *ngIf="showDetail === true">
          Phone Number: {{contact.phone}}<br>
          Email: {{contact.email}}
        </div>
    `,
    styleUrls: ["../src/css/app.css"]
})
export class AppComponent {
  public contact = { firstName: "Brian", lastName: "Paintsil", phone: "233 20 8953410", email: "brnpaintsil@gmail.com"};
  public showDetail = false;

  onSelect(){
    this.showDetail = true;
  }
}

